const jwt = require('jsonwebtoken')
const {
    RANDOM
} = require('mysql/lib/PoolSelector')

function generateAccessToken(email) {

    const jti = RANDOM(32).toString('hex')

    const jwtToken = jwt.sign({
            jti,
            email,
        },
        process.env.SECRET, {
            expiresIn: '1 day',
        }
    )

    return jwtToken
}

module.exports = {
    generateAccessToken
}