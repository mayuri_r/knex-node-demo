const express = require("express");
const UsersController = require('../controllers/user.controller')
const asyncWrap = require('express-async-wrapper')
const authorization = require('../../auth/auth.middleware')
const validator = require('express-joi-validation').createValidator({})
const validation = require('../dtos/users.dto')
const router = express.Router();


router.get('/users', authorization, asyncWrap(UsersController.users))
router.get('/users/:userId', authorization, asyncWrap(UsersController.getUserById))
router.post('/register', validator.body(validation), asyncWrap(UsersController.createUser));
router.post('/login', asyncWrap(UsersController.login))
router.put('/update/:userId', validator.body(validation), authorization, asyncWrap(UsersController.updateUser))
router.delete('/delete/:userId', authorization, asyncWrap(UsersController.deleteUser))

module.exports = router;