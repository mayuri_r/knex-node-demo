const options = require('../../../database.config');
const {
    generateAccessToken
} = require('../../auth/auth');
const knex = require('knex')(options);
const bcrypt = require('bcrypt')
const passport = require('passport')

//  class UsersServices {

/**
 * get all users
 */

async function users() {
    return knex('users')
}

/**
 * get user by id
 * @param {*} user 
 */

async function getUserById(user) {
    const findUser = await knex('users').where('id', user.userId).first()
    if(!findUser){
        throw new Error('user not found')
    }
    return findUser
}

/**
 * add user
 */

async function createUser(user) {
    await knex('users').insert({
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        password: await bcrypt.hash(user.password, 10)
    })
}

/**
 * Login
 */

async function login(user) {

    const userdata = await knex('users').where({
        email: user.email
    }).first()

    if (!userdata) {
        throw new Error('email is wrong')
    }
    const pass = await bcrypt.compare(user.password,userdata.password)

    if (!pass) {
        throw new Error('password is wrong')
    }

   
    return userdata
}

/**
 * update User
 */

async function updateUser(userId, user) {
    const checkUser = await knex('users').where('id', userId)
    if (!checkUser.length) {
        throw new Error('User Not Found')
    }
    await knex('users').where('id', userId)
        .update({
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            password: user.password
        })

}

/**
 * delete user
 */

async function deleteUser(userId) {
    const checkUser = await knex('users').where('id', userId)
    if (!checkUser.length) {
        throw new Error('User Not Found')
    }

    await knex('users').where('id', userId).del()
}

module.exports = {
    users,
    getUserById,
    createUser,
    login,
    updateUser,
    deleteUser
};