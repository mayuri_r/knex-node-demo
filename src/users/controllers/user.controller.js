const user = require('../services/user.service');
const jwt = require('jsonwebtoken');
const { generateAccessToken } = require('../../auth/auth');
// class UsersController {

/**
 * get all users
 * @param {*} req
 * @param {*} res
 */

async function users(req, res) {
  const users = await user.users()
  res.json({
    message: "all users",
    users
  });
}

/**
 * get user by id
 * @param {*} req
 * @param {*} res
 */

async function getUserById(req, res) {
  const data = await user.getUserById(req.params)
  res.json({
    data
  })
}

/**
 * createUser
 * @param {*} req
 * @param {*} res
 */

async function createUser(req, res) {
  await user.createUser(req.body)

  const token =  generateAccessToken(req.body.email)

  res.json({
    message: "user created suceessfully",
    token
  })
}

/**
 * login
 * @param {*} req 
 * @param {*} res 
 */

async function login(req,res) {
  await user.login(req.body)
  const token =  generateAccessToken(req.body.email)
  res.json({
    message:"login suceessfull",
    token
  })
}

/**
 * update User
 * @param {*} req
 * @param {*} res
 */

async function updateUser(req, res) {
  await user.updateUser(req.params.userId, req.body)
  res.json({
    message: "user updated suceessfully"
  })
}

/**
 * delete user
 * @param {*} req
 * @param {*} res
 */

async function deleteUser(req, res) {
  await user.deleteUser(req.params.userId)
  res.json({
    message: "user deleted suceessfully"
  })
}

module.exports = {
  users,
  getUserById,
  createUser,
  login,
  updateUser,
  deleteUser
};