const express = require('express')
const userRoute = require('./src/users/routes/user.route')
const app = express()
const bodyParser = require('body-parser')

app.use(express.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.use('/user',userRoute)

app.listen(process.env.APP_PORT,() => console.log("listion on servre",process.env.APP_PORT))