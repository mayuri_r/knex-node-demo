require("dotenv").config();

module.exports = {
  client: process.env.CLIENT,
  port: process.env.PORT,
  version: '8.0',
  connection: {
    host: process.env.HOST,
    user: process.env.DATABASEUSER,
    password: process.env.PASSWORD,
    database: process.env.DATABASE,
  },
  migrations: {
    directory: `${__dirname}/migrations`,
  },
  seeds: {
    directory: `${__dirname}/seeds`,
  },
  pool: {
    min: 2,
    max: 10,
  },
}